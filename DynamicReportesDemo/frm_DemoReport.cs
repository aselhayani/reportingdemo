﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DynamicReportesDemo
{
    public partial class frm_DemoReport : DevExpress.XtraEditors.XtraForm
    {
        public frm_DemoReport()
        {
            InitializeComponent();
        }
        void GetDate()
        {

            var db =new  DynamicDataContext();
            var salestable = db.SALESTABLEs.Select(x => x);
            var TransTable = db.INVENTTRANs.Where(x=>x.PROJID != string.Empty).Select(x => x);
            if (radioGroup1.SelectedIndex == 1)
            {
                if (dateEdit1.DateTime.Year > 1970)
                    salestable = salestable.Where(x => x.DELIVERYDATE >= dateEdit1.DateTime);
                if (dateEdit2.DateTime.Year > 1970)
                    salestable = salestable.Where(x => x.DELIVERYDATE <= dateEdit2.DateTime);

                TransTable = TransTable.Where(x => salestable.Select(xst => xst.SALESID).Contains(x.TRANSREFID));
            }
            else
            {
                if (dateEdit1.DateTime.Year > 1970)
                    TransTable = TransTable.Where(x => x.DATEPHYSICAL >= dateEdit1.DateTime);
                if (dateEdit2.DateTime.Year > 1970)
                    TransTable = TransTable.Where(x => x.DATEPHYSICAL <= dateEdit2.DateTime);
            }
            int stateFilter = 0;
            if (int.TryParse(comboBoxEdit1.Text, out stateFilter))
                TransTable = TransTable.Where(x => x.STATUSISSUE == stateFilter);



                var ruselt   = (from inTr in TransTable 
                                join pr in db.PROJTABLEs on inTr.PROJID equals pr.PROJID 
                                        select new
                                        {
                                         inTr.ITEMID ,
                                         inTr.PROJID ,    
                                         inTr.TRANSREFID  ,
                                         inTr.STATUSISSUE ,
                                         Branch = pr.DIMENSION ,
                                         Department =pr.DIMENSION2_ ,
                                         Employees = pr.DIMENSION3_ ,
                                         inTr.DATEPHYSICAL  ,
                                         //DateDiff =Convert.ToInt32( (inTr.DATEPHYSICAL - ((DateTime?)salestable.FirstOrDefault(x => x.SALESID == inTr.TRANSREFID).DELIVERYDATE)).Value.).ToString() +" Days",
                                         //db.SALESLINEs.First(x => inTr.TRANSREFID == x.SALESID && inTr.PROJID == x.PROJID && x.ITEMID == inTr.ITEMID).SALESUNIT,
                                         TransType = (inTr.QTY < 0)? "-":"+" ,
                                         QTY=Math.Abs( inTr.QTY ),
                                         //Total = Convert.ToDouble(Math.Abs(((((decimal?)db.SALESLINEs.First(x => inTr.TRANSREFID == x.SALESID && inTr.PROJID == x.PROJID && x.ITEMID == inTr.ITEMID).SALESPRICE)??0) * inTr.QTY)  )),
                                         inTr.COSTAMOUNTPHYSICAL,
                                        }).ToList();
            var project = ruselt.Select(x => x.PROJID).Distinct().ToList();
            var transferIDs = ruselt.Select(x => x.TRANSREFID).Distinct().ToList();
            var salesLine = db.SALESLINEs.Where(x => project.Contains(x.PROJID)).ToList();
            var salesTable = db.SALESTABLEs .Where(x => transferIDs.Contains(x.SALESID )).ToList();
            var result2 = ruselt.Select(rs => new
            {
                rs.PROJID,
                rs.TRANSREFID,
                rs.ITEMID,
                ItemName = salesLine.Where(x =>   x.ITEMID == rs.ITEMID).Select(x=>x.NAME).FirstOrDefault()??"",
                rs.Branch ,
                rs.Employees ,
                rs.Department ,
                StatusIssue =  rs.STATUSISSUE,
                DeliveryDate = salesTable.Where (x => x.SALESID == rs.TRANSREFID).Select(x=> (DateTime?)x.DELIVERYDATE ).FirstOrDefault(),
                PhysicalDate =  rs.DATEPHYSICAL, 
                SalesUnit = salesLine.Where(x => rs.TRANSREFID == x.SALESID && rs.PROJID == x.PROJID && x.ITEMID == rs.ITEMID).Select(x => x.SALESUNIT ).FirstOrDefault() ?? "",
                rs.TransType,
                rs.QTY,
                SalesPrice  =salesLine.Where(x => rs.TRANSREFID == x.SALESID && rs.PROJID == x.PROJID && x.ITEMID == rs.ITEMID).Select(x =>(decimal ?) x.SALESPRICE ).FirstOrDefault() ?? 0,
              //  CostPrice = salesLine.Where(x => rs.TRANSREFID == x.SALESID && rs.PROJID == x.PROJID && x.ITEMID == rs.ITEMID).Select(x => (decimal?)x.cos ).FirstOrDefault() ?? 0,
                PhysicalCost  =  rs.COSTAMOUNTPHYSICAL,
                
            }).ToList();
            gridControl1.DataSource = result2;
          
            if(CustomColumnsAdded == false)
            {
                gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
                {
                    VisibleIndex = 16,
                    Name = "clmTotalPrice",
                    FieldName = "TotalPrice",
                    UnboundType = DevExpress.Data.UnboundColumnType.Decimal,
                    UnboundExpression = "[SalesPrice] * [QTY]"
                });
                gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
                {
                    VisibleIndex = 9,
                    Name = "clmDateDiff",
                    FieldName = "DateDiff",
                    UnboundType = DevExpress.Data.UnboundColumnType.Decimal,
                    UnboundExpression = "DateDiffDay([DeliveryDate],[PhysicalDate])", 

                });
                CustomColumnsAdded = true ;
            }

            gridView1.Columns["TransType"].Width = 50;
            gridView1.Columns["StatusIssue"].Width = 50;

            gridView1.Columns["PhysicalCost"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gridView1.Columns["PhysicalCost"].DisplayFormat.FormatString   = "N2";
            gridView1.Columns["TotalPrice"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gridView1.Columns["TotalPrice"].DisplayFormat.FormatString = "N2";
            gridView1.Columns["QTY"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gridView1.Columns["QTY"].DisplayFormat.FormatString = "N2";
            gridView1.Columns["SalesPrice"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gridView1.Columns["SalesPrice"].DisplayFormat.FormatString = "N2";



        }
        bool CustomColumnsAdded;
       

         

        private void frm_DemoReport_Load(object sender, EventArgs e)
        {
            dateEdit1.DateTime = new DateTime(DateTime.Now.Year, 1, 1);
            dateEdit2.DateTime = DateTime.Now;
            var db = new DynamicDataContext();
            comboBoxEdit1.Properties.Items.AddRange(db.INVENTTRANs .Select(x=>x.STATUSISSUE ).Distinct().ToList());
         //   comboBoxEdit1.Text = "2";
            gridView1.CustomDrawCell += GridView1_CustomDrawCell;
          

        }



        private void GridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
           if(e.Column.FieldName == "TransType")
            {
               if(e.CellValue is string s)
                {
                    if (s == "+")
                        e.Appearance.BackColor = Color.Green;
                    else
                        e.Appearance.BackColor = Color.Red;
                }
            }
            if (e.Column.FieldName == "StatusIssue")
            {
                if (e.CellValue is int s)
                {
                    if (s ==2)
                    {
                        e.Appearance.BackColor = Color.LightGray ;
                        e.DisplayText = "Deducted";
                    }
                    else if(s == 1)
                    {
                        e.Appearance.BackColor = Color.LightGreen ;
                        e.DisplayText = "Sold";

                    }
                }
            }
        }

        private void comboBoxEdit1_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
                comboBoxEdit1.Text = "";
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetDate();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount == 0)
                XtraMessageBox.Show(noRowsErrorText, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                rpt_GridReport.Print(this.gridControl1, "Sales Report Demo ", "", false, rpt_GridReport.PrintType.Preview );
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount == 0)
                XtraMessageBox.Show(noRowsErrorText, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                rpt_GridReport.Print(this.gridControl1, "Sales Report Demo ", "", false, rpt_GridReport.PrintType.Print );
            }
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount == 0) 
                XtraMessageBox.Show(noRowsErrorText , "",MessageBoxButtons.OK,MessageBoxIcon.Exclamation ); 
            else
            {
                rpt_GridReport.Print(this.gridControl1, "Sales Report Demo ", "", false, rpt_GridReport.PrintType.ExportToXlsx );
            }
        }
        string noRowsErrorText { get => "Grid doesn't contains any rows \n Please use different Filter Criterion  "; }
    }
}