﻿using System;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System.IO;
using DevExpress.XtraEditors;
using System.Diagnostics;

namespace DynamicReportesDemo
{
    public partial class rpt_GridReport : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt_GridReport()
        {
            InitializeComponent();
        }
        public enum PrintType
        {
            Preview ,
            Print,
            PrintDialog, 
            ExportToXlsx,

        }
        public static void Print(GridControl gridControl1, string ReportName, string filter, bool rtll, PrintType  printType)
        {

            GridView view = gridControl1.MainView as GridView;


            PrintableComponentLink pcLink1 = new PrintableComponentLink();
            pcLink1.Component = gridControl1;
            rpt_GridReport rpt = new rpt_GridReport();
          //  rpt.LoadTemplete();


            rpt.printableComponentContainer1.PrintableComponent = pcLink1;
            try
            {
             //   rpt.xrPictureBox1.Image = MasterClass.GetImageFromByte(CurrentSession.Company.Imge.ToArray());
            }
            catch (Exception)
            {
            }

            rpt.lbl_filter.Text = filter; 

            rpt.lbl_PrinDate.Text = DateTime.Now.ToString(); 
            rpt.lbl_UserName.Text = "User name goes here";
            rpt.lbl_companyName.Text = "ATTIEH MEDICO Ltd";


            rpt.lbl_rptName.Text = ReportName;

            switch (printType)
            {
                case PrintType.Preview:
                    rpt.ShowPreview();
                    break;
                case PrintType.Print:
                    rpt.Print();
                    break;
                case PrintType.PrintDialog:
                    rpt.PrintDialog();
                    break;
                case PrintType.ExportToXlsx:
                    using (XtraSaveFileDialog dialog = new XtraSaveFileDialog() { Filter = "Xlsx files (*.xlsx)|*.xlsx" ,RestoreDirectory = true  })
                    {
                        if(dialog.ShowDialog()== System.Windows.Forms.DialogResult.OK)
                        {
                            rpt.ExportToXlsx(dialog.FileName);
                            Debug.Print(dialog.FileName);
                           // Debug.Print(dialog.OpenFile//);
                        }
                    }
                    break;
                default:
                    break;
            }
            
            //switch (CurrentSession.user.WhenPrintShowMode)
            //{
            //    case 0:  break;
            //    case 1: rpt.PrintDialog(); break;
            //    case 2: rpt.Print(); break;
            //    default: rpt.PrintDialog(); break;
            //}
        }

    }
}
